﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Runtime.CompilerServices;
using System.Threading;
using System.Web.Script.Serialization;
using System.Windows.Forms;

namespace CurrentlyPlayingMSN
{
    internal class Program
    {        
        public static void Main(string[] args)
        {
            // https://accounts.spotify.com/authorize?response_type=code&client_id=...&scope=user-read-currently-playing&redirect_uri=...

            var refreshToken = File.ReadAllText("refresh_token");
            Console.WriteLine("Refresh token: {0}", refreshToken);
            
            WebClient refresher = new WebClient();
            
            var authToken = refresher.DownloadString(
                "https://.../callback.php?code=" + refreshToken + "&refresh=true");

            refresher.Dispose();
            
            Console.WriteLine("Auth token: {0}", authToken);
            
            WebClient wc = new WebClient();
            wc.Headers.Add("Authorization", "Bearer " + authToken);

            while (true)
            {
                var res = "";
                
                try
                {
                    res = wc.DownloadString("https://api.spotify.com/v1/me/player/currently-playing");

                    if (res != "")
                    {
                        // code from stackoverflow
                        var serializer = new JavaScriptSerializer();
                        serializer.RegisterConverters(new[] {new DynamicJsonConverter()});

                        dynamic currentSong = serializer.Deserialize(res, typeof(object));

                        if ((bool) currentSong.is_playing == true)
                        {

                            var item = currentSong.item;

                            var artistList = new List<string>();

                            foreach (var artistObj in item.album.artists)
                            {
                                artistList.Add(artistObj.name);
                            }

                            var albumName = (string) item.album.name;
                            var artist = string.Join(", ", artistList.ToArray());
                            var name = (string) item.name;

                            // code from gist
                            MsnStatus.SetMusic(artist, name, albumName);

                            Console.WriteLine("Set to: {0} ({1}) - {2}", artist, albumName, name);
                        }
                        else
                        {
                            MsnStatus.Clear();
                            Console.WriteLine("Nothing is playing");
                        }
                    }
                }
                catch (WebException)
                {
                    // probably expired
                    // /shrug

                    Console.WriteLine("Token expired (exception)!");
                    Process.Start(Application.ExecutablePath);
                    Thread.Sleep(3000);
                    Environment.Exit(0);
                    
                    break;
                }

                System.Threading.Thread.Sleep(10000);
            }
        }
    }
}