<?php
	// stolen from stackoverflow
	function encodeURIComponent($str) {
	    $revert = array('%21'=>'!', '%2A'=>'*', '%27'=>"'", '%28'=>'(', '%29'=>')');
	    return strtr(rawurlencode($str), $revert);
	}

	$redirectUri = 'https://.../callback.php';
	$clientId = '';
	$clientSecret = '';

	$curl = curl_init();

	if(!isset($_GET['refresh'])) {
	  	curl_setopt($curl, CURLOPT_URL,            'https://accounts.spotify.com/api/token' );
	  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1 );
	  	curl_setopt($curl, CURLOPT_POST,           1 );
	  	curl_setopt($curl, CURLOPT_POSTFIELDS,     'grant_type=authorization_code&code=' . encodeURIComponent($_GET['code']) . '&redirect_uri=' . encodeURIComponent($redirectUri) ); 
	  	curl_setopt($curl, CURLOPT_HTTPHEADER,     array('Authorization: Basic '.base64_encode($clientId.':'.$clientSecret)) ); 

		$result = json_decode(curl_exec($curl));

		echo $result->refresh_token;
	} else {
		// refresh

	  	curl_setopt($curl, CURLOPT_URL,            'https://accounts.spotify.com/api/token' );
	  	curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1 );
	  	curl_setopt($curl, CURLOPT_POST,           1 );
	  	curl_setopt($curl, CURLOPT_POSTFIELDS,     'grant_type=refresh_token&refresh_token=' . encodeURIComponent($_GET['code']) ); 
	  	curl_setopt($curl, CURLOPT_HTTPHEADER,     array('Authorization: Basic '.base64_encode($clientId.':'.$clientSecret)) ); 

		$result = json_decode(curl_exec($curl));

		echo $result->access_token;
	}

	curl_close($curl);