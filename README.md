# How to use

1. Create an app on Spotify
2. Host the PHP file and edit the `$clientId` and `$clientSecret` variables (use Heroku if you don't have a host)
3. Modify the redirect URI to be where `callback.php` is located
4. Get your refresh token from `https://accounts.spotify.com/authorize?response_type=code&client_id=[clientId]&scope=user-read-currently-playing&redirect_uri=[exact uri-encoded redirect URI from the Spotify dashboard]` (it'll redirect)
5. Modify the C# Program.cs file and PHP callback to have the right endpoints (callback URL)
6. Compile the C# project
7. Place your refresh token from step 4 in a file named `refresh_token`

# Note

This is extremely bad code. It was made to work _good enough_ for my use. Therefore, it has 0 error handling since the only user would be me and I could debug the issue.

# Credits

* JSON Deserializer - https://stackoverflow.com/a/3806407 (StackOverflow/Drew Noakes)
* MsnStatus class - modified from https://gist.github.com/JulioC/2729712 (GitHub/JulioC)
* PHP access token retriever - modified from https://gist.github.com/ahallora/4aac6d048742d5de0e65 (GitHub/ahallora)